﻿using UnityEngine;
using System.Collections;

public class MovingDown : MonoBehaviour
{
	public float moveDownSpeed;

	private Rigidbody rbody;

	void Start ()
	{
		rbody = GetComponent<Rigidbody>();
	}

	void FixedUpdate ()
	{
		Vector3 offset = new Vector3 (0f, -moveDownSpeed, 0f);
		rbody.MovePosition (transform.position + offset);
	}
}
