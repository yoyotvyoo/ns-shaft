﻿using UnityEngine;
using System.Collections;

public class SpawnPlatform : MonoBehaviour
{
	public GameObject[] spawnObjs;

	public float MinX;
	public float MaxX;
	//public float MinY;
	//public float MaxY;
	public float MinZ;
	public float MaxZ;

	public float topDownDistance;

	private float spawnHeight = -2.0f;

	void Start ()
	{
		InvokeRepeating ("SpawnObject", 0, 1f);

		// start scene
		while (spawnHeight > -topDownDistance)
		{
			float x = Random.Range (MinX, MaxX);
			float y = spawnHeight;
			float z = Random.Range (MinZ, MaxZ);

			spawnHeight -= 2;

			int platformIndex = Random.Range (0, 5);

			Instantiate (spawnObjs[platformIndex], new Vector3(x, y, z), Quaternion.identity);
		}

	}

	/*
	void Update ()
	{

	}
	*/

	void SpawnObject ()
	{
		float x = Random.Range (MinX, MaxX);
		//float y = Random.Range (MinY, MaxY);
		float z = Random.Range (MinZ, MaxZ);

		int platformIndex = Random.Range (0, 5);

		Instantiate (spawnObjs[platformIndex], new Vector3(x, spawnHeight, z), Quaternion.identity);
		spawnHeight -= 2;

	}
}
