﻿using UnityEngine;
using System.Collections;

public class BasicEffect : MonoBehaviour
{
	public AudioSource landSound;

	void OnCollisionEnter(Collision col)
	{
		if (col.collider.tag == "Player")
		{
			landSound.Play ();
		}

		if (col.collider.tag == "TopSpikes")
		{
			Destroy (gameObject);
		}
	}
}
