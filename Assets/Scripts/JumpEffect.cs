﻿using UnityEngine;
using System.Collections;

public class JumpEffect : MonoBehaviour 
{
	public AudioSource jumpSound;
	public float jumpSpeed;
	private Rigidbody rbody;

	void OnCollisionEnter(Collision col)
	{
		if (col.collider.tag == "Player")
		{
			rbody = col.gameObject.GetComponent<Rigidbody> ();

			/*
			if (rbody.velocity.y >= jumpSpeed)
				rbody.velocity = new Vector3 (rbody.velocity.x, jumpSpeed, rbody.velocity.z);
			*/

			rbody.velocity = new Vector3 (rbody.velocity.x, jumpSpeed, rbody.velocity.z);

			jumpSound.Play ();
		}

		if (col.collider.tag == "TopSpikes")
		{
			Destroy (gameObject);
		}
	}
}
