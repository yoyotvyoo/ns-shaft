﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DeathZone : MonoBehaviour
{
	public AudioSource dieSound1;


	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			dieSound1.Play ();	

			Invoke ("LoadMainMenu", 2.0f);
		}
	}

	void LoadMainMenu()
	{
		SceneManager.LoadScene (0);
	}
}
