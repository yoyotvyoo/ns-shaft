﻿using UnityEngine;
using System.Collections;

public class FlipEffect : MonoBehaviour
{
	public AudioSource flipSound;

	void OnCollisionEnter(Collision col)
	{
		if (col.collider.tag == "Player")
		{
			flipSound.Play ();
		}

		if (col.collider.tag == "TopSpikes")
		{
			Destroy (gameObject);
		}
	}
}
