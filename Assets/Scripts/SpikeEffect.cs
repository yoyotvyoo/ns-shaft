﻿using UnityEngine;
using System.Collections;

public class SpikeEffect : MonoBehaviour
{
	public AudioSource spikeSound;

	void OnCollisionEnter(Collision col)
	{
		if (col.collider.tag == "Player")
		{
			spikeSound.Play ();
		}

		if (col.collider.tag == "TopSpikes")
		{
			Destroy (gameObject);
		}
	}
}
