﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public Transform target;

	//private Animator animator;
	private Rigidbody rbody;

	void Start ()
	{
		Cursor.lockState = CursorLockMode.Locked;
		rbody = GetComponent<Rigidbody>();
		//animator = GetComponent<Animator>();
	}

	void FixedUpdate ()
	{
		float moveVertical = Input.GetAxis ("Vertical") * speed * Time.deltaTime;
		float moveHorizontal = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;

		// animation
		//animator.SetBool ("isRun", Input.GetButtonDown("Vertical") || Input.GetButtonDown("Horizontal"));

		// move
		rbody.MovePosition (transform.position + transform.forward * moveVertical + transform.right * moveHorizontal);

		// facing direction
		Vector3 lookDirection = target.forward;
		lookDirection.y = 0;
		transform.rotation = Quaternion.LookRotation (lookDirection);

		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Cursor.lockState = CursorLockMode.None;
		}
	}
}
