﻿using UnityEngine;
using System.Collections;

public class MoveEffect : MonoBehaviour 
{
	public AudioSource moveSound;
	public float moveSpeed;
	private Rigidbody rbody;

	void OnCollisionEnter(Collision col)
	{
		if (col.collider.tag == "Player")
		{
			moveSound.Play ();
		}

		if (col.collider.tag == "TopSpikes")
		{
			Destroy (gameObject);
		}
	}
		
	void OnCollisionStay(Collision col)
	{
		if (col.collider.tag == "Player")
		{
			rbody = col.gameObject.GetComponent<Rigidbody> ();
			rbody.velocity = moveSpeed * Vector3.forward;
		}
	}
}
